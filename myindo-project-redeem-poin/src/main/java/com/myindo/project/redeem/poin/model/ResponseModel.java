/**
 * 
 */
package com.myindo.project.redeem.poin.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseModel {
	private String responseCode;
	private String responseMessage;
	private String date;
	private String time;
	ResponseContentModel contentModel;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public ResponseContentModel getContentModel() {
		return contentModel;
	}
	public void setContentModel(ResponseContentModel contentModel) {
		this.contentModel = contentModel;
	}
}
