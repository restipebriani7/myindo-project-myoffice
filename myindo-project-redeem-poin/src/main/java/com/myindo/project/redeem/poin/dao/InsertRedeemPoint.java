/**
 * 
 */
package com.myindo.project.redeem.poin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Random;
import java.util.logging.Logger;
import com.myindo.project.redeem.poin.connection.Connect;
import com.myindo.project.redeem.poin.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class InsertRedeemPoint {
	Logger log = Logger.getLogger("InsertRedeemPoint");
	
	public String redeemDb(RequestModel rModel) throws Exception{
		String codeResponse = "";
		try {
			Connection con = Connect.connection();
			Random random = new Random();
			String hsl = ""+random.nextInt();
			PreparedStatement pStatement = con.prepareStatement("INSERT INTO poin (id_poin, total_poin) VALUES (?,?)");
			pStatement.setString(1, hsl);
			pStatement.setString(2, rModel.getTotalPoint());
			
			pStatement.executeUpdate();
			pStatement.close();
			codeResponse = "00";
			log.info("Input redeem poin success");
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "01";
			log.info("Input redeem poin failed");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
