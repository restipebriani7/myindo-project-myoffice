/**
 * 
 */
package com.myindo.project.redeem.poin.connection;

import java.sql.Connection;
import java.util.logging.Logger;

import org.postgresql.ds.PGSimpleDataSource;

/**
 * @author Resti Pebriani
 *
 */
public class Connect {
	
	static Logger log = Logger.getLogger("Connect");
	public static Connection con;
	public static Connection connection() {
		try {
			PGSimpleDataSource dataSource = new PGSimpleDataSource();
			dataSource.setDatabaseName("DB_HCGS");
			dataSource.setPassword("postgres");
			dataSource.setPortNumber(5432);
			dataSource.setServerName("postgres");
			dataSource.setUser("172.17.3.152");
			
			con = dataSource.getConnection();
			log.info("Connect is Success");
		} catch (Exception e) {
			// TODO: handle exception
			log.info("COnnect is Failed");
			log.info(e.getMessage());
		}
		return con;
	}
}
