/**
 * 
 */
package com.myindo.project.redeem.poin.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String totalPoint;
	private String pointRedeem;
	private String redeemDescription;
	
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getTotalPoint() {
		return totalPoint;
	}
	public void setTotalPoint(String totalPoint) {
		this.totalPoint = totalPoint;
	}
	public String getPointRedeem() {
		return pointRedeem;
	}
	public void setPointRedeem(String pointRedeem) {
		this.pointRedeem = pointRedeem;
	}
	public String getRedeemDescription() {
		return redeemDescription;
	}
	public void setRedeemDescription(String redeemDescription) {
		this.redeemDescription = redeemDescription;
	}	
}
