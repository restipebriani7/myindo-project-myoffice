/**
 * 
 */
package com.myindo.project.redeem.poin.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String dateRedeem;
	private String message;
	public String getDateRedeem() {
		return dateRedeem;
	}
	public void setDateRedeem(String dateRedeem) {
		this.dateRedeem = dateRedeem;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
