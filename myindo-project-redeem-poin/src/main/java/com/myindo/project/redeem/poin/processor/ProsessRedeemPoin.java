/**
 * 
 */
package com.myindo.project.redeem.poin.processor;

import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.redeem.poin.dao.InsertRedeemPoint;
import com.myindo.project.redeem.poin.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsessRedeemPoin implements Processor{
	Logger log = Logger.getLogger("ProsessRedeemPoin");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		InsertRedeemPoint iPoint = new InsertRedeemPoint();
		
		String codeResponse = iPoint.redeemDb(rModel);
		System.out.println("Coderesponse : "+codeResponse);
		
		String res = response(codeResponse);
		
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
		
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}
