/**
 * 
 */
package com.myindo.project.listcuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
