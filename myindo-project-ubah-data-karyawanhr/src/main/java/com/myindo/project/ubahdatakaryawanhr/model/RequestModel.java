/**
 * 
 */
package com.myindo.project.ubahdatakaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String nama;
	private String status_karyawan;
	private String tgl_bergabung;
	private String tgl_resign;
	private String id_jabatan;
	private String id_department;
	private String tmpt_lahir;
	private String tgl_lahir;
	private String alamat_ktp;
	private String alamat_skrg;
	private String agama;
	private String status_pernikahan;
	private String jns_kelamin;
	private String gol_darah;
	private String no_telp;
	private String email;
	private String npwp;
	private String id_asuransi;
	private String id_tabungan;
	private String id_pendidikan;
	private String role;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getStatus_karyawan() {
		return status_karyawan;
	}
	public void setStatus_karyawan(String status_karyawan) {
		this.status_karyawan = status_karyawan;
	}
	public String getTgl_bergabung() {
		return tgl_bergabung;
	}
	public void setTgl_bergabung(String tgl_bergabung) {
		this.tgl_bergabung = tgl_bergabung;
	}
	public String getTgl_resign() {
		return tgl_resign;
	}
	public void setTgl_resign(String tgl_resign) {
		this.tgl_resign = tgl_resign;
	}
	public String getId_jabatan() {
		return id_jabatan;
	}
	public void setId_jabatan(String id_jabatan) {
		this.id_jabatan = id_jabatan;
	}
	public String getId_department() {
		return id_department;
	}
	public void setId_department(String id_department) {
		this.id_department = id_department;
	}
	public String getTmpt_lahir() {
		return tmpt_lahir;
	}
	public void setTmpt_lahir(String tmpt_lahir) {
		this.tmpt_lahir = tmpt_lahir;
	}
	public String getTgl_lahir() {
		return tgl_lahir;
	}
	public void setTgl_lahir(String tgl_lahir) {
		this.tgl_lahir = tgl_lahir;
	}
	public String getAlamat_ktp() {
		return alamat_ktp;
	}
	public void setAlamat_ktp(String alamat_ktp) {
		this.alamat_ktp = alamat_ktp;
	}
	public String getAlamat_skrg() {
		return alamat_skrg;
	}
	public void setAlamat_skrg(String alamat_skrg) {
		this.alamat_skrg = alamat_skrg;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getStatus_pernikahan() {
		return status_pernikahan;
	}
	public void setStatus_pernikahan(String status_pernikahan) {
		this.status_pernikahan = status_pernikahan;
	}
	public String getJns_kelamin() {
		return jns_kelamin;
	}
	public void setJns_kelamin(String jns_kelamin) {
		this.jns_kelamin = jns_kelamin;
	}
	public String getGol_darah() {
		return gol_darah;
	}
	public void setGol_darah(String gol_darah) {
		this.gol_darah = gol_darah;
	}
	public String getNo_telp() {
		return no_telp;
	}
	public void setNo_telp(String no_telp) {
		this.no_telp = no_telp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getId_asuransi() {
		return id_asuransi;
	}
	public void setId_asuransi(String id_asuransi) {
		this.id_asuransi = id_asuransi;
	}
	public String getId_tabungan() {
		return id_tabungan;
	}
	public void setId_tabungan(String id_tabungan) {
		this.id_tabungan = id_tabungan;
	}
	public String getId_pendidikan() {
		return id_pendidikan;
	}
	public void setId_pendidikan(String id_pendidikan) {
		this.id_pendidikan = id_pendidikan;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}	
}
