/**
 * 
 */
package com.myindo.project.ubahdatakaryawanhr.processor;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.ubahdatakaryawanhr.dao.UpdateDataKaryawanHr;
import com.myindo.project.ubahdatakaryawanhr.model.RequestModel;
import com.myindo.project.ubahdatakaryawanhr.model.ResponseContentModel;
import com.myindo.project.ubahdatakaryawanhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProcessUbahDataKaryawanHr implements Processor {
	ObjectMapper mapper = new ObjectMapper();
	Logger log = Logger.getLogger("ProcessUbahDataKaryawanHr");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		RequestModel requestModel = gson.fromJson(body, RequestModel.class);

		UpdateDataKaryawanHr updateDataKaryawanHr = new UpdateDataKaryawanHr();

		String codeResponse = updateDataKaryawanHr.updateDb(requestModel);
		System.out.println("CodeResponse : " + codeResponse);

		String strReq = response(codeResponse);

		Response response = Response.status(HttpStatus.OK_200).entity(strReq)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		ResponseModel responseModel = new ResponseModel();
		ResponseContentModel contentModel = new ResponseContentModel();

		try {
			if (codeResponse.equals("0000")) {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				contentModel.setMessage("Data Berhasil Diubah");
			} else if (codeResponse.equals("1111")) {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setMessage("Data Gagal Diubah");
			}

			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int mount = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);
			
			contentModel.setCode(codeResponse);
			responseModel.setDate(("" + year + "-" + mount + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}

		return response;
	}

}
