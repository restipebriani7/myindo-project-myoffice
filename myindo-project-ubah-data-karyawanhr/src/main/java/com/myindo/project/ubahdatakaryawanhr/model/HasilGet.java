/**
 * 
 */
package com.myindo.project.ubahdatakaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String npk;
	
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
}
