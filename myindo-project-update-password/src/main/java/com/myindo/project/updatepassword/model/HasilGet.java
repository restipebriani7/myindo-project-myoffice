/**
 * 
 */
package com.myindo.project.updatepassword.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String npk;

	public String getNpk() {
		return npk;
	}

	public void setNpk(String npk) {
		this.npk = npk;
	}
}
