/**
 * 
 */
package com.myindo.project.updatepassword.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.updatepassword.dao.UpdatePass;
import com.myindo.project.updatepassword.model.RequestModel;
import com.myindo.project.updatepassword.model.ResponseContentModel;
import com.myindo.project.updatepassword.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsessUpdate implements Processor{
	Logger log = Logger.getLogger("ProsessUpdate");
	ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		UpdatePass updatePass = new UpdatePass();
		
		String codeResponse = updatePass.update(rModel);
		String str = response(codeResponse);
		
		Response response = Response.status(HttpStatus.OK_200).entity(str).header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		ResponseModel responseModel = new ResponseModel();
		ResponseContentModel contentModel = new ResponseContentModel();
		
		try {
			if (codeResponse.equals("0000")) {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				contentModel.setMessage("Password Berhasil Diubah");
			} else{
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setMessage("Password Gagal Diubah");
			}

			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int mount = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);

			contentModel.setCode(codeResponse);
			responseModel.setDate(("" + year + "-" + mount + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}
}
