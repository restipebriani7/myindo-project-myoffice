/**
 * 
 */
package com.myindo.project.updatepassword.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.myindo.project.updatepassword.connection.Connect;
import com.myindo.project.updatepassword.model.HasilGet;
import com.myindo.project.updatepassword.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class UpdatePass {
	Logger log = Logger.getLogger("UpdatePass");
	String codeResponse;
	HasilGet getHasil;

	public String update(RequestModel rModel) {
		Connection con = Connect.connection();
		getHasil = new HasilGet();

		try {
			PreparedStatement po = con.prepareStatement("SELECT npk FROM karyawan WHERE npk=?");
			po.setString(1, rModel.getNpk());
			ResultSet ro = po.executeQuery();
			while (ro.next()) {
				getHasil.setNpk(ro.getString("npk"));
			}
			ro.close();

			if (!getHasil.getNpk().trim().isEmpty() && getHasil.getNpk() != null) {
				PreparedStatement ps = con.prepareStatement("UPDATE karyawan SET password=? WHERE npk=?");
				ps.setString(1, rModel.getPass());
				ps.setString(2, rModel.getNpk());

				ps.executeUpdate();
				ps.close();
				codeResponse = "0000";
				log.info("Update password berhasil");
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Update password gagal");
			log.info(e.getStackTrace() + "");
		}
		return codeResponse;
	}
}
