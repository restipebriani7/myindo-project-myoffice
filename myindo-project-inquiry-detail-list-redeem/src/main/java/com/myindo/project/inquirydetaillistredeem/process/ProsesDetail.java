/**
 * 
 */
package com.myindo.project.inquirydetaillistredeem.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquirydetaillistredeem.dao.GetDetail;
import com.myindo.project.inquirydetaillistredeem.model.HasilGet;
import com.myindo.project.inquirydetaillistredeem.model.RequestModel;
import com.myindo.project.inquirydetaillistredeem.model.ResponseContentModel;
import com.myindo.project.inquirydetaillistredeem.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesDetail implements Processor{
	Logger log = Logger.getLogger("ProsesDetail");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		
		GetDetail getDetail = new GetDetail();
		String codeResponse = getDetail.detailRedeem(rModel);
		
		HasilGet hasilGet = gson.fromJson(codeResponse, HasilGet.class);
		
		String str = response(hasilGet);
		Response response = Response.status(HttpStatus.OK_200).entity(str).header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(HasilGet hasilGet) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel = new ResponseContentModel();
			if (!hasilGet.getNpk().trim().isEmpty()) {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				contentModel.setNpk(hasilGet.getNpk());
				contentModel.setIdProduct(hasilGet.getIdProduct());
				contentModel.setProductName(hasilGet.getProductName());
				contentModel.setAmount(hasilGet.getAmount());
				contentModel.setStatus(hasilGet.getStatus());
				contentModel.setDateTrans(hasilGet.getDateTrans());
			} else {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setNpk("");
				contentModel.setIdProduct("");
				contentModel.setProductName("");
				contentModel.setAmount("");
				contentModel.setStatus("");
				contentModel.setDateTrans("");
			}
			
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

}
