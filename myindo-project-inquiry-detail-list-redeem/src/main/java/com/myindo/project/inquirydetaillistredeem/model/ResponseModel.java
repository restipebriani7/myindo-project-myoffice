/**
 * 
 */
package com.myindo.project.inquirydetaillistredeem.model;


/**
 * @author Resti Pebriani
 *
 */
public class ResponseModel {
	private String responseCode;
	private String responseMessage;
	private String date;
	private String time;
	ResponseContentModel content;
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public ResponseContentModel getContent() {
		return content;
	}
	public void setContent(ResponseContentModel content) {
		this.content = content;
	}
}
