/**
 * 
 */
package com.myindo.project.inquirydetaillistredeem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirydetaillistredeem.connection.Connect;
import com.myindo.project.inquirydetaillistredeem.model.HasilGet;
import com.myindo.project.inquirydetaillistredeem.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetDetail {
	Logger log = Logger.getLogger("GetDetail");
	HasilGet resultDB ;
	String codeResponse ;
	String hasilGet;
	
	public String detailRedeem(RequestModel model) throws Exception {
		Connection con = Connect.connection();
		resultDB = new HasilGet();
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT npk, id_product, date_trans, status FROM list_redeem WHERE npk=? AND id_list=?");
			ps.setString(1, model.getNpk());
			ps.setString(2, model.getIdList());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				resultDB.setNpk(rs.getString("npk"));
				resultDB.setIdProduct(rs.getString("id_product"));
				resultDB.setDateTrans(rs.getString("date_trans"));
				resultDB.setStatus(rs.getString("status"));
			}
			rs.close();
			System.out.println("Npk : " +model.getNpk());
			
			if (!resultDB.getIdProduct().trim().isEmpty() && resultDB.getIdProduct() != null) {
				PreparedStatement po = con.prepareStatement("SELECT product_name, amount FROM product WHERE id_product=?");
				po.setString(1, resultDB.getIdProduct());
				ResultSet ro = po.executeQuery();
				while (ro.next()) {
					resultDB.setProductName(ro.getString("product_name"));
					resultDB.setAmount(ro.getString("amount"));
				}
				ro.close();
			}
			
			ObjectMapper mapper = new ObjectMapper();
			hasilGet = mapper.writeValueAsString(resultDB);
			System.out.println("Hasil get data : " + hasilGet);
			log.info("Inquiry detail list redeem sukses");
			
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Inquiry detail list redeem gagal");
			log.info(e.getMessage());
			
			resultDB.setNpk("");
			resultDB.setIdProduct("");
			resultDB.setProductName("");
			resultDB.setAmount("");
			resultDB.setDateTrans("");
			resultDB.setStatus("");
			
			ObjectMapper mapper = new ObjectMapper();
			hasilGet = mapper.writeValueAsString(resultDB);
			System.out.println("Hasil get data : " + hasilGet);
		}
		return hasilGet;
	}
}
