/**
 * 
 */
package com.myindo.project.inquirylistpengajuancutiatasan.processor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquirylistpengajuancutiatasan.dao.GetListPecutiAtasan;
import com.myindo.project.inquirylistpengajuancutiatasan.model.HasilModel;
import com.myindo.project.inquirylistpengajuancutiatasan.model.RequestModel;
import com.myindo.project.inquirylistpengajuancutiatasan.model.ResponseContentModel;
import com.myindo.project.inquirylistpengajuancutiatasan.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProccessInquiryCutiAtasan implements Processor {
	Logger log = Logger.getLogger("ProccessInquiryCutiAtasan");
	ObjectMapper mapper = new ObjectMapper();

	GetListPecutiAtasan gAtasan = new GetListPecutiAtasan();

	String codeResponse;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel rModel = gson.fromJson(body, RequestModel.class);

		ArrayList<HasilModel> hasilModel = gAtasan.inquiryCutiAtasan(rModel);

		if (hasilModel.isEmpty()) {
			codeResponse ="1111";
			log.info("Get data gagal");
		} else {
			codeResponse ="0000";
			log.info("Get data berhasil");
		}
		
		String sRes = response(codeResponse, hasilModel);
		Response response = Response.status(HttpStatus.OK_200).entity(sRes)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse, ArrayList<HasilModel> hasilModel) {
		// TODO Auto-generated method stub
		String response = "";

		try {
			ResponseModel responseModel = new ResponseModel();
			ArrayList<ResponseContentModel> rContentModels = new ArrayList<>();

			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilModel.size(); i++) {
					ResponseContentModel rContentModel = new ResponseContentModel();
					responseModel.setResponseMessage("Success");
					rContentModel.setId_pengajuan(hasilModel.get(i).getId_pengajuan());
					rContentModel.setStatus(hasilModel.get(i).getStatus_pengajuan());
					rContentModel.setDateOfPaidLeave(hasilModel.get(i).getTgl_cuti());
					rContentModel.setTotalDays(hasilModel.get(i).getJml_hari());
					rContentModel.setDateOfEntry(hasilModel.get(i).getTgl_masuk());
					rContentModel.setReason(hasilModel.get(i).getKet_cuti());
					rContentModel.setNpk(hasilModel.get(i).getNpk());

					rContentModels.add(rContentModel);
					log.info("Respon : " + rContentModels.toString());
				}
			} else {
				ResponseContentModel rModel = new ResponseContentModel();
				responseModel.setResponseMessage("Failed");
				rContentModels.add(rModel);
			}
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContentModel(rContentModels);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			/* TODO: handle exception */
			log.info(e.getStackTrace() + "");
		}
		return response;
	}
}
