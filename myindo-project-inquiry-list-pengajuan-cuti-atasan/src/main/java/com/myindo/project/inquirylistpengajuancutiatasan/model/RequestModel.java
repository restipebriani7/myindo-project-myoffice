/**
 * 
 */
package com.myindo.project.inquirylistpengajuancutiatasan.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	public String status;
	public String cc_atasan;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCc_atasan() {
		return cc_atasan;
	}
	public void setCc_atasan(String cc_atasan) {
		this.cc_atasan = cc_atasan;
	}
}
