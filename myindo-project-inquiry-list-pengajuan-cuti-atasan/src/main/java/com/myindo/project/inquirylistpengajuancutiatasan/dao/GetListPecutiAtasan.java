/**
 * 
 */
package com.myindo.project.inquirylistpengajuancutiatasan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.inquirylistpengajuancutiatasan.connection.Connect;
import com.myindo.project.inquirylistpengajuancutiatasan.model.HasilModel;
import com.myindo.project.inquirylistpengajuancutiatasan.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetListPecutiAtasan {
	Logger log = Logger.getLogger("GetListPecutiAtasan");
	String codeResponse = "";
	
	public ArrayList<HasilModel> inquiryCutiAtasan(RequestModel rModel) throws Exception{
		ArrayList<HasilModel> hasilModels = new ArrayList<>();
		HasilModel hModel = new HasilModel();
		Connection con = Connect.connection();
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM pengajuan_cuti WHERE status_pengajuan=? AND cc_atasan=?");
			ps.setString(1, rModel.getStatus());
			ps.setString(2, rModel.getCc_atasan());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hModel = new HasilModel();
				hModel.setId_pengajuan(rs.getString("id_pengajuan"));
				hModel.setStatus_pengajuan(rs.getString("status_pengajuan"));
				hModel.setTgl_cuti(rs.getString("tgl_cuti"));
				hModel.setJml_hari(rs.getString("jml_hari"));
				hModel.setKet_cuti(rs.getString("ket_cuti"));
				hModel.setTgl_masuk(rs.getString("tgl_masuk"));
				hModel.setNpk(rs.getString("npk"));
				hModel.setCc_atasan(rs.getString("cc_atasan"));
				hModel.setCc_hr(rs.getString("cc_hr"));
				
				/*
				 * Date tglCuti = rs.getDate("tgl_cuti"); 
				 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				 * String dateCuti = dateFormat.format(tglCuti);
				 * System.out.println(""+hModel.getTgl_cuti());
				 * 
				 * hModel.setTgl_cuti(dateCuti);
				 */
				
				hasilModels.add(hModel);
			}
			rs.close();
			System.out.println("Status : "+rModel.getStatus());
			System.out.println("CC : " +rModel.getCc_atasan());
			System.out.println("Hasil Models : " +hasilModels.toString());
			codeResponse = "0000";
			log.info("Get data berhasil");
			
			con.close(); 
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Get data gagal");
			log.info(e.getMessage());
		}
		return hasilModels;
	}
}
