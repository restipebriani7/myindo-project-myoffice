/**
 * 
 */
package com.myindo.project.inquirylistpengajuancutiatasan.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String status;
	private String dateOfPaidLeave;
	private String totalDays;
	private String dateOfEntry;
	private String reason;
	private String npk;
	private String id_pengajuan;
	
	public String getId_pengajuan() {
		return id_pengajuan;
	}
	public void setId_pengajuan(String id_pengajuan) {
		this.id_pengajuan = id_pengajuan;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateOfPaidLeave() {
		return dateOfPaidLeave;
	}
	public void setDateOfPaidLeave(String dateOfPaidLeave) {
		this.dateOfPaidLeave = dateOfPaidLeave;
	}
	public String getTotalDays() {
		return totalDays;
	}
	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	@Override
	public String toString() {
		return "ResponseContentModel [id_pengajuan=" + id_pengajuan + ", status=" + status + ", dateOfPaidLeave="
				+ dateOfPaidLeave + ", totalDays=" + totalDays + ", dateOfEntry=" + dateOfEntry + ", reason=" + reason
				+ ", npk=" + npk + "]";
	}
}
