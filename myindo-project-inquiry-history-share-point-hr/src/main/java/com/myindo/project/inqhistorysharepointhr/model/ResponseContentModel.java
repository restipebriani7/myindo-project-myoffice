/**
 * 
 */
package com.myindo.project.inqhistorysharepointhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String id_share;
	private String id_point_hr;
	private String point_out;
	private String dateTransaction;
	private String description;
	private String to;
	public String getId_share() {
		return id_share;
	}
	public void setId_share(String id_share) {
		this.id_share = id_share;
	}
	public String getPoint_out() {
		return point_out;
	}
	public void setPoint_out(String point_out) {
		this.point_out = point_out;
	}
	public String getDateTransaction() {
		return dateTransaction;
	}
	public void setDateTransaction(String dateTransaction) {
		this.dateTransaction = dateTransaction;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId_point_hr() {
		return id_point_hr;
	}
	public void setId_point_hr(String id_point_hr) {
		this.id_point_hr = id_point_hr;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
}
