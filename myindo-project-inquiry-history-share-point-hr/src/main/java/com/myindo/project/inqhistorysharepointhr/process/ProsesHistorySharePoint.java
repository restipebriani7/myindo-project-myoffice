/**
 * 
 */
package com.myindo.project.inqhistorysharepointhr.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inqhistorysharepointhr.dao.GetHistorySharePointHr;
import com.myindo.project.inqhistorysharepointhr.model.HasilGet;
import com.myindo.project.inqhistorysharepointhr.model.RequestModel;
import com.myindo.project.inqhistorysharepointhr.model.ResponseContentModel;
import com.myindo.project.inqhistorysharepointhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesHistorySharePoint implements Processor{
	Logger log = Logger.getLogger("ProsesHistorySharePoint");
	ObjectMapper mapper = new ObjectMapper();
	String codeResponse;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		GetHistorySharePointHr sharePointHr = new GetHistorySharePointHr();
		
//		String hasil = sharePointHr.inquiryHistory(rModel);
		ArrayList<HasilGet> hasilGets = sharePointHr.inquiryHistory(rModel);
//		HasilGet historyPoint = gson.fromJson(hasil, HasilGet.class);
//		System.out.println("Result : "+historyPoint);
		
		if (hasilGets.isEmpty()) {
			codeResponse ="1111";
			log.info("Get data gagal");
		} else {
			codeResponse ="0000";
			log.info("Get data berhasil");
		}
		
		String res = response(codeResponse, hasilGets);
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse, ArrayList<HasilGet> hasilGets) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel responseModel = new ResponseModel();
			ArrayList<ResponseContentModel> contentModel = new ArrayList<>();
			
			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilGets.size(); i++) {
					ResponseContentModel model = new ResponseContentModel();
					responseModel.setResponseMessage("Success");
					model.setId_share(hasilGets.get(i).getId_share());
					model.setId_point_hr(hasilGets.get(i).getIdPointHr());
					model.setPoint_out(hasilGets.get(i).getPoint_out());
					model.setDateTransaction(hasilGets.get(i).getDateTransaction());
					model.setDescription(hasilGets.get(i).getDescription());
					model.setTo(hasilGets.get(i).getTo());
					
					contentModel.add(model);
				}
			} else {
				ResponseContentModel model = new ResponseContentModel();
				responseModel.setResponseMessage("Failed");
				model.setId_share("");
				model.setId_point_hr("");
				model.setPoint_out("");
				model.setDateTransaction("");
				model.setDescription("");
				model.setTo("");
				
				contentModel.add(model);
			}
			
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);
			
			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);
			
			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
