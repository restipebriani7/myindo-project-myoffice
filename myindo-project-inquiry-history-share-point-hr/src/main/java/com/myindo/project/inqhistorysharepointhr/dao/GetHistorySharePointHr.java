/**
 * 
 */
package com.myindo.project.inqhistorysharepointhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.inqhistorysharepointhr.connection.Connect;
import com.myindo.project.inqhistorysharepointhr.model.HasilGet;
import com.myindo.project.inqhistorysharepointhr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetHistorySharePointHr {
	Logger log = Logger.getLogger("GetHistorySharePointHr");
	HasilGet listPoint;
	String hasilGet;

	public ArrayList<HasilGet> inquiryHistory(RequestModel rModel) throws Exception {
		ArrayList<HasilGet> hArrayList = new ArrayList<>();
		Connection con = Connect.connection();

		try {
			PreparedStatement ps = con.prepareStatement("SELECT id_point_hr, npk FROM point_hr WHERE npk=?");
			ps.setString(1, rModel.getNpk());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listPoint = new HasilGet();
				listPoint.setIdPointHr(rs.getString("id_point_hr"));
				listPoint.setNpk(rs.getString("npk"));

			}
			rs.close();
			System.out.println("Npk : " + rModel.getNpk());

			if (!listPoint.getNpk().trim().isEmpty() && listPoint.getNpk() != null) {
				log.info("Select data point_hr berdasarkan npk berhasil");

				PreparedStatement pp = con.prepareStatement("SELECT * FROM share_hr WHERE id_point_hr=?");
				pp.setString(1, listPoint.getIdPointHr());
				ResultSet rr = pp.executeQuery();
				while (rr.next()) {
					listPoint = new HasilGet();
					listPoint.setId_share(rr.getString(1));
					listPoint.setIdPointHr(rr.getString(2));
					listPoint.setPoint_out(rr.getString(3));
					listPoint.setDateTransaction(rr.getString(4));
					listPoint.setDescription(rr.getString(5));
					listPoint.setTo(rr.getString(6));

					hArrayList.add(listPoint);

				}
				rr.close();
//				ObjectMapper mapper = new ObjectMapper();
//				hasilGet = mapper.writeValueAsString(listPoint);
//				System.out.println("Hasil get data : "+hasilGet);

				con.close();
			} else {
				log.info("Select data gagal");
			}

			System.out.println("Array : " + hArrayList.toString());
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Failed");
			log.info(e.getMessage());

			listPoint.setId_share("");
			listPoint.setIdPointHr("");
			listPoint.setPoint_out("");
			listPoint.setDateTransaction("");
			listPoint.setDescription("");
			listPoint.setTo("");
//			
//			ObjectMapper mapper = new ObjectMapper();
//			hasilGet = mapper.writeValueAsString(listPoint);
//			System.out.println("Hasil catch data : "+hasilGet);
		}
		return hArrayList;
	}
}
