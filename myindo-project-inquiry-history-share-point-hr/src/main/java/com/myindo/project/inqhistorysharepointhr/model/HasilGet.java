/**
 * 
 */
package com.myindo.project.inqhistorysharepointhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String idPointHr;
	private String npk;
	private String id_share;
	private String total_point;
	private String point_out;
	private String dateTransaction;
	private String description;
	private String to;
	
	public String getIdPointHr() {
		return idPointHr;
	}
	public void setIdPointHr(String idPointHr) {
		this.idPointHr = idPointHr;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getId_share() {
		return id_share;
	}
	public void setId_share(String id_share) {
		this.id_share = id_share;
	}
	public String getTotal_point() {
		return total_point;
	}
	public void setTotal_point(String total_point) {
		this.total_point = total_point;
	}
	public String getPoint_out() {
		return point_out;
	}
	public void setPoint_out(String point_out) {
		this.point_out = point_out;
	}
	public String getDateTransaction() {
		return dateTransaction;
	}
	public void setDateTransaction(String dateTransaction) {
		this.dateTransaction = dateTransaction;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	@Override
	public String toString() {
		return "HasilGet [idPointHr=" + idPointHr + ", npk=" + npk + ", id_share=" + id_share + ", total_point="
				+ total_point + ", point_out=" + point_out + ", dateTransaction=" + dateTransaction + ", description="
				+ description + ", to=" + to + "]";
	}
}
