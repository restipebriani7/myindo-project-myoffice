/**
 * 
 */
package com.myindo.project.pengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String message;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
