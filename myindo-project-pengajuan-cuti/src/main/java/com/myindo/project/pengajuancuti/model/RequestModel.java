/**
 * 
 */
package com.myindo.project.pengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String name;
	private String position;
	private String department;
	private String dateOfPaidLeave;
	private String totalDays;
	private String reason;
	private String dateOfEntry;
	private String ccAtasan;
	private String ccHr;
	private String status;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDateOfPaidLeave() {
		return dateOfPaidLeave;
	}
	public void setDateOfPaidLeave(String dateOfPaidLeave) {
		this.dateOfPaidLeave = dateOfPaidLeave;
	}
	public String getTotalDays() {
		return totalDays;
	}
	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public String getCcAtasan() {
		return ccAtasan;
	}
	public void setCcAtasan(String ccAtasan) {
		this.ccAtasan = ccAtasan;
	}
	public String getCcHr() {
		return ccHr;
	}
	public void setCcHr(String ccHr) {
		this.ccHr = ccHr;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
