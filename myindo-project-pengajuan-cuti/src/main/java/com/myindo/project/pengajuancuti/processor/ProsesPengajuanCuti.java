/**
 * 
 */
package com.myindo.project.pengajuancuti.processor;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.pengajuancuti.dao.InsertPengajuanCuti;
import com.myindo.project.pengajuancuti.model.RequestModel;
import com.myindo.project.pengajuancuti.model.ResponseContentModel;
import com.myindo.project.pengajuancuti.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesPengajuanCuti implements Processor{
	ObjectMapper mapper= new ObjectMapper();
	Logger log = Logger.getLogger("ProsesPengajuanCuti");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		InsertPengajuanCuti cuti = new InsertPengajuanCuti();
		
		String codeResponse = cuti.addCuti(requestModel);
		
		System.out.println("CodeResponse : " +codeResponse);
		
		String str = response(codeResponse);
		Response response = Response.status(HttpStatus.OK_200)
				.entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json")
				.build();
		exchange.getOut().setBody(response);
	}
	
	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel rModel = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();
			if (codeResponse.equals("0000")) {
				rModel.setResponseMessage("Success");
				rContentModel.setMessage("Pengajuan cuti sukses. Menunggu approval");
			}else if (codeResponse.equals("1111")) {
				rModel.setResponseMessage("Failed");
				rContentModel.setMessage("Pengajuan cuti gagal");
			}
			GregorianCalendar gCalendar = new GregorianCalendar();
			
			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH)+1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);
			
			rModel.setResponseCode(codeResponse);
			rModel.setDate(("" + year + "-" + month + "-" + day).toString());
			rModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			rModel.setContent(rContentModel);
			
			response = mapper.writeValueAsString(rModel);	
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}
		return response;
	}
}
