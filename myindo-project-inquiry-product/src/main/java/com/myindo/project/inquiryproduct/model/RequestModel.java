/**
 * 
 */
package com.myindo.project.inquiryproduct.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String idProduct;

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
}
