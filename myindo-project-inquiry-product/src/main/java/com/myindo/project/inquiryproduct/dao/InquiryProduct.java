/**
 * 
 */
package com.myindo.project.inquiryproduct.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.inquiryproduct.connection.Connect;
import com.myindo.project.inquiryproduct.model.HasilData;
import com.myindo.project.inquiryproduct.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class InquiryProduct {
	Logger log = Logger.getLogger("InquiryProduct");
	HasilData hasilData;
	String result;

	public ArrayList<HasilData> inquiryProduct(RequestModel rModel) throws Exception {
		ArrayList<HasilData> arrayData = new ArrayList<>();
		Connection con = Connect.connection();

		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM product");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hasilData = new HasilData();
				hasilData.setIdProduct(rs.getString("id_product"));
				hasilData.setProductName(rs.getString("product_name"));
				hasilData.setAmount(rs.getString("amount"));
				
				arrayData.add(hasilData);
			}
			rs.close();
			System.out.println("Hasil array : "+arrayData.toString());
			log.info("Inquiry produk sukses");
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Inquiry produk failed");
			log.info(e.getMessage());
			hasilData = new HasilData();

			hasilData.setProductName("");
			hasilData.setAmount("");
		}
		return arrayData;
	}
}
