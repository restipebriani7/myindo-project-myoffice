/**
 * 
 */
package com.myindo.project.inquiryproduct.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String idProduct;
	private String productName;
	private String amount;
	
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ResponseContentModel [idProduct=" + idProduct + ", productName=" + productName + ", amount=" + amount
				+ "]";
	}
}
