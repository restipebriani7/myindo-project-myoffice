/**
 * 
 */
package com.myindo.project.inquiryproduct.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquiryproduct.dao.InquiryProduct;
import com.myindo.project.inquiryproduct.model.HasilData;
import com.myindo.project.inquiryproduct.model.RequestModel;
import com.myindo.project.inquiryproduct.model.ResponseContentModel;
import com.myindo.project.inquiryproduct.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesInquiryProduct implements Processor {
	Logger log = Logger.getLogger("ProsesInquiryProduct");
	ObjectMapper mapper = new ObjectMapper();
	String codeResponse;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		InquiryProduct iProduct = new InquiryProduct();
		ArrayList<HasilData> hasilArray = iProduct.inquiryProduct(requestModel);
		
		if (hasilArray.isEmpty()) {
			codeResponse ="1111";
			log.info("Inquiry data gagal");
		} else {
			codeResponse ="0000";
			log.info("Inquiry data berhasil");
		}
		
		String ser = response(hasilArray);
		Response response = Response.status(HttpStatus.OK_200).entity(ser)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(ArrayList<HasilData> hasilArray) {
		// TODO Auto-generated method stub
		String response = "";

		try {
			ResponseModel responseModel = new ResponseModel();
			ArrayList<ResponseContentModel> rContentModel = new ArrayList<>();

			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilArray.size(); i++) {
					ResponseContentModel contentModel = new ResponseContentModel();
					responseModel.setResponseMessage("Success");
					contentModel.setIdProduct(hasilArray.get(i).getIdProduct());
					contentModel.setProductName(hasilArray.get(i).getProductName());
					contentModel.setAmount(hasilArray.get(i).getAmount());

					rContentModel.add(contentModel);
				}
			} else {
				ResponseContentModel model = new ResponseContentModel();
				responseModel.setResponseMessage("Failed");
				model.setIdProduct("");
				model.setProductName("");
				model.setAmount("");
				
				rContentModel.add(model);
			}
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);
			
			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(rContentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
