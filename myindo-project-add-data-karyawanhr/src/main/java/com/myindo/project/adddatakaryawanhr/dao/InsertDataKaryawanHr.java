/**
 * 
 */
package com.myindo.project.adddatakaryawanhr.dao;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import com.myindo.project.adddatakaryawanhr.connection.Connect;
import com.myindo.project.adddatakaryawanhr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class InsertDataKaryawanHr {
	Logger log = Logger.getLogger("InsertDataKaryawanHr");
	String path;
	int uniqueID;

//	public static String md5Java(String message) {
//		String digest = null;
//		try {
//			MessageDigest md = MessageDigest.getInstance("SHA-1");
//			byte[] hash = md.digest(message.getBytes("UTF-8"));
//			
//			// merubah byte array ke dalam String Hexadecimal
//			StringBuilder sb = new StringBuilder(2 * hash.length);
//			for (byte b : hash) {
//				sb.append(String.format("%02x", b & 0xff));
//			}
//			digest = sb.toString();
//		} catch (Exception e) {
//			// TODO: handle exception
//			System.out.println("Error");
//		}
//		return digest;
//	}

	public String addDb(RequestModel reModel) throws Exception {
		Connection con = (Connection) Connect.connection();
		String codeResponse = "";

		try {
//			//encrypt password
//			String original = reModel.getPass();
//			String encrypt = md5Java(original);
//			System.out.println("Pass original : "+original);
//			System.out.println("MD5 Hashing : "+encrypt);

			// penamaan foto
//			Random random =  new Random();
//			int foto = random.nextInt(100000);
//			String fotoString = Integer.toString(foto);
//	        
//	      //decode base64 to image and save to directory
//			String base64String = reModel.getPhoto();
//	     
//			byte[] data = DatatypeConverter.parseBase64Binary(base64String);
//			String path ="C:\\Users\\Resti Pebriani\\workspace\\picture\\KRYW" +fotoString+ ".png";
//	        File file = new File(path);
//	        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))){
//				outputStream.write(data);
//			} catch (IOException e) {
//				// TODO: handle exception
//				System.out.println("Error : "+e.getStackTrace());
//			}
			if (!reModel.getNpk().trim().isEmpty() && reModel != null) {
				PreparedStatement ps = con
						.prepareStatement("INSERT INTO karyawan(npk, nama, status_karyawan, tgl_bergabung, "
								+ "tgl_resign, id_jabatan, id_department, tmpt_lahir, tgl_lahir, alamat_ktp, alamat_skrg, agama, "
								+ "status_pernikahan, jns_kelamin, gol_darah, no_telp, email, npwp, total_poin, total_cuti, id_asuransi, "
								+ "id_tabungan, id_pendidikan, password, role) "
								+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
				ps.setString(1, reModel.getNpk());
				ps.setString(2, reModel.getNama());
				ps.setString(3, reModel.getStatus_karyawan());
				ps.setString(4, reModel.getTgl_bergabung());
				ps.setString(5, reModel.getTgl_resign());
				ps.setString(6, reModel.getId_jabatan());
				ps.setString(7, reModel.getId_department());
				ps.setString(8, reModel.getTmpt_lahir());
				ps.setString(9, reModel.getTgl_lahir());
				ps.setString(10, reModel.getAlamat_ktp());
				ps.setString(11, reModel.getAlamat_skrg());
				ps.setString(12, reModel.getAgama());
				ps.setString(13, reModel.getStatus_pernikahan());
				ps.setString(14, reModel.getJns_kelamin());
				ps.setString(15, reModel.getGol_darah());
				ps.setString(16, reModel.getNo_telp());
				ps.setString(17, reModel.getEmail());
				ps.setString(18, reModel.getNpwp());
				ps.setString(19, reModel.getTotal_poin());
				ps.setString(20, reModel.getTotal_cuti());
				ps.setString(21, reModel.getId_asuransi());
				ps.setString(22, reModel.getId_tabungan());
				ps.setString(23, reModel.getId_pendidikan());
				ps.setString(24, reModel.getPassword());
				ps.setString(25, reModel.getRole());

				ps.executeUpdate();
				ps.close();
				codeResponse = "0000";
				log.info("Insert Data ke tabel karyawan berhasil");
				System.out.println("CodeResponse : " + codeResponse);
			}else {
				codeResponse = "1111";
				log.info("Data tidak boleh kosong");
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Insert Data is gagal");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
