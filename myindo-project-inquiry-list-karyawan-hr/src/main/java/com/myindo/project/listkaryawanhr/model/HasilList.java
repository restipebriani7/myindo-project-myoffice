/**
 * 
 */
package com.myindo.project.listkaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilList {
	private String npk;
	private String nama;
	private String jabatan;
	private String department;
	private String photo;
	
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJabatan() {
		return jabatan;
	}
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	@Override
	public String toString() {
		return "HasilList [npk=" + npk + ", nama=" + nama + ", jabatan=" + jabatan + ", department=" + department
				+ ", photo=" + photo + "]";
	}
}
