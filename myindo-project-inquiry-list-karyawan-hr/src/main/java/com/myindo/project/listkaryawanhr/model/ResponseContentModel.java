/**
 * 
 */
package com.myindo.project.listkaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String npk;
	private String name;
	private String position;
	private String department;
	private String photo;
	
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
