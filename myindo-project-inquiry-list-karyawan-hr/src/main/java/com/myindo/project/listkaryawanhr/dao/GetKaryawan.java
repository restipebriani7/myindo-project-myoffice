/**
 * 
 */
package com.myindo.project.listkaryawanhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.listkaryawanhr.connection.Connect;
import com.myindo.project.listkaryawanhr.model.HasilList;
import com.myindo.project.listkaryawanhr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetKaryawan {
	Logger log = Logger.getLogger("GetKaryawan");
	HasilList hasilList;
	String codeResponse = "";
	
	public ArrayList<HasilList> inquiryListKar(RequestModel rModel) {
		ArrayList<HasilList> hArrayList = new ArrayList<>();
		Connection con = Connect.connection();
		
		try {
			PreparedStatement pp = con.prepareStatement("SELECT * FROM karyawan");
			ResultSet rs = pp.executeQuery();
			while (rs.next()) {
				hasilList = new HasilList();
				hasilList.setNpk(rs.getString("npk"));
				hasilList.setNama(rs.getString("nama"));
				hasilList.setJabatan(rs.getString("id_jabatan"));
				hasilList.setDepartment(rs.getString("id_department"));
				hasilList.setPhoto(rs.getString("foto"));
				
				hArrayList.add(hasilList);
			}
			rs.close();
			System.out.println("Hasil array : "+hArrayList.toString());
			codeResponse = "0000";
			log.info("List karyawan berhasil ditampilkan");
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("List karyawan gagal ditampilkan");
			log.info(e.getMessage());
			
			hasilList.setNpk("");
			hasilList.setNama("");
			hasilList.setJabatan("");
			hasilList.setDepartment("");
			hasilList.setPhoto("");
		}
		return hArrayList;	
	}
}
