/**
 * 
 */
package com.myindo.project.listkaryawanhr.proses;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.listkaryawanhr.dao.GetKaryawan;
import com.myindo.project.listkaryawanhr.model.HasilList;
import com.myindo.project.listkaryawanhr.model.RequestModel;
import com.myindo.project.listkaryawanhr.model.ResponseContentModel;
import com.myindo.project.listkaryawanhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesListKaryawan implements Processor {
	Logger log = Logger.getLogger("ProsesListKaryawan");
	ObjectMapper mapper = new ObjectMapper();
	String codeResponse = "";
	String encodstring = "";

	private FileInputStream fileInputStreamReader;

	private String encodeFileToBase64Binary(File file) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Encode File To Base64 Binary");
		fileInputStreamReader = new FileInputStream(file);
		byte[] bytes = new byte[(int) file.length()];
		fileInputStreamReader.read(bytes);
		System.out.println("Sukses encode");
		return new String(org.postgresql.util.Base64.encodeBytes(bytes));
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel reModel = gson.fromJson(body, RequestModel.class);

		GetKaryawan karyawan = new GetKaryawan();

		ArrayList<HasilList> hasilLists = karyawan.inquiryListKar(reModel);
		log.info("Array : " + hasilLists);

		if (hasilLists.isEmpty()) {
			codeResponse = "1111";
			log.info("List karyawan gagal di tampilkan");
		} else {
			codeResponse = "0000";
			log.info("List karyawan berhasil di tampilkan");
		}

		String res = response(codeResponse, hasilLists);
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse, ArrayList<HasilList> hasilLists) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel responseModel = new ResponseModel();
			ArrayList<ResponseContentModel> contentModel = new ArrayList<>();

			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilLists.size(); i++) {
					ResponseContentModel rContentModel = new ResponseContentModel();
					responseModel.setResponseMessage("Success");

					String foto = hasilLists.get(i).getPhoto();
					File f = new File(foto);
					encodstring = encodeFileToBase64Binary(f);

					rContentModel.setNpk(hasilLists.get(i).getNpk());
					rContentModel.setName(hasilLists.get(i).getNama());
					rContentModel.setPosition(hasilLists.get(i).getJabatan());
					rContentModel.setDepartment(hasilLists.get(i).getDepartment());

					rContentModel.setPhoto(encodstring);

					contentModel.add(rContentModel);
				}
			} else {
				ResponseContentModel model = new ResponseContentModel();
				responseModel.setResponseMessage("Failed");
				contentModel.add(model);
			}

			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}

}
