/**
 * 
 */
package com.myindo.project.login.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;

	public String getNpk() {
		return npk;
	}

	public void setNpk(String npk) {
		this.npk = npk;
	}
}
