/**
 * 
 */
package com.myindo.project.login.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String npk;
	private String nama;
	private String role;
	private String passwd;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
}
