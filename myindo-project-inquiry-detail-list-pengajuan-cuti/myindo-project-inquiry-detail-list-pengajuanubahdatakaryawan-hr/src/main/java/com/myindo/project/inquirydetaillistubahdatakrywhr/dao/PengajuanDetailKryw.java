package com.myindo.project.inquirydetaillistubahdatakrywhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirydetaillistubahdatakrywhr.connection.Connect;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.HasilGet;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.RequestModel;

public class PengajuanDetailKryw {
	Logger log = Logger.getLogger("PengajuanDetailKryw");
	HasilGet hKaryawan;
	String hasilGet;
	
	public String detailKryw (RequestModel model) throws Exception {
		Connection con = Connect.connection();
		hKaryawan = new HasilGet();
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM karyawan_temp WHERE npk=?");
			ps.setString(1, model.getNpk());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hKaryawan.setNpk(rs.getString("npk"));
				hKaryawan.setName(rs.getString("nama"));
				hKaryawan.setBirthPlace(rs.getString("tmpt_lahir"));
				hKaryawan.setBirthDate(rs.getString("tgl_lahir"));
				hKaryawan.setKtpAddress(rs.getString("alamat_ktp"));
				hKaryawan.setDomicileAddress(rs.getString("alamat_skrg"));
				hKaryawan.setReligion(rs.getString("agama"));
				hKaryawan.setMaritalStatus(rs.getString("status_pernikahan"));
				hKaryawan.setGender(rs.getString("jns_kelamin"));
				hKaryawan.setBloodType(rs.getString("gol_darah"));
				hKaryawan.setPhoneNumber(rs.getString("no_telp"));
				hKaryawan.setEmail(rs.getString("email"));
				hKaryawan.setIdEducation(rs.getString("id_pendidikan"));
			}
			rs.close();
			
			System.out.println("Npk : " +model.getNpk());
			
			if (!hKaryawan.getNpk().trim().isEmpty() && hKaryawan.getNpk() != null) {
				ObjectMapper mapper = new ObjectMapper();
				hasilGet = mapper.writeValueAsString(hKaryawan);
				System.out.println("Hasil Get Karyawan : "+hasilGet);
				
				log.info("Select ubah data karyawan berhasil");
				con.close();
			} else {
				log.info("Select ubah data karyawan gagal");
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Gagal");
			log.info(e.getMessage());
			
			hKaryawan.setNpk("");
			hKaryawan.setName("");
			hKaryawan.setBirthPlace("");
			hKaryawan.setBirthDate("");
			hKaryawan.setKtpAddress("");
			hKaryawan.setDomicileAddress("");
			hKaryawan.setReligion("");
			hKaryawan.setMaritalStatus("");
			hKaryawan.setGender("");
			hKaryawan.setBloodType("");
			hKaryawan.setPhoneNumber("");
			hKaryawan.setEmail("");
			hKaryawan.setIdEducation("");
			
			ObjectMapper mapper = new ObjectMapper();
			hasilGet = mapper.writeValueAsString(hKaryawan);
			System.out.println("Hasil Get catch karyawan : " + hasilGet);
		}
		return hasilGet;
	}
}
