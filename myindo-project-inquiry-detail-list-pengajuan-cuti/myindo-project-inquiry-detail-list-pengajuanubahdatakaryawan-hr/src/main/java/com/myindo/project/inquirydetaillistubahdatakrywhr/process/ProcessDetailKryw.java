package com.myindo.project.inquirydetaillistubahdatakrywhr.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquirydetaillistubahdatakrywhr.dao.PengajuanDetailKryw;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.HasilGet;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.RequestModel;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.ResponseContentModel;
import com.myindo.project.inquirydetaillistubahdatakrywhr.model.ResponseModel;

public class ProcessDetailKryw implements Processor{
	Logger log = Logger.getLogger("ProcessDetailKryw");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		PengajuanDetailKryw detailKryw = new PengajuanDetailKryw();
		
		String codeResponse = detailKryw.detailKryw(requestModel);
		HasilGet hasilKaryawan = gson.fromJson(codeResponse, HasilGet.class);
		
		String str = response(hasilKaryawan);
		Response response = Response.status(HttpStatus.OK_200).entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(HasilGet hasilKaryawan) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel rModel = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();
			
			if (!hasilKaryawan.getNpk().trim().isEmpty() && hasilKaryawan.getNpk() != null) {
				rModel.setResponseCode("0000");
				rModel.setResponseMessage("Success");
				rContentModel.setNpk(hasilKaryawan.getNpk());
				rContentModel.setName(hasilKaryawan.getName());
				rContentModel.setBirthPlace(hasilKaryawan.getBirthPlace());
				rContentModel.setBirthDate(hasilKaryawan.getBirthDate());
				rContentModel.setKtpAddress(hasilKaryawan.getKtpAddress());
				rContentModel.setDomicileAddress(hasilKaryawan.getDomicileAddress());
				rContentModel.setReligion(hasilKaryawan.getReligion());
				rContentModel.setMaritalStatus(hasilKaryawan.getMaritalStatus());
				rContentModel.setBloodType(hasilKaryawan.getBloodType());
				rContentModel.setGender(hasilKaryawan.getGender());
				rContentModel.setPhoneNumber(hasilKaryawan.getPhoneNumber());
				rContentModel.setEmail(hasilKaryawan.getEmail());
				rContentModel.setIdEducation(hasilKaryawan.getIdEducation());
			} else {
				rModel.setResponseCode("1111");
				rModel.setResponseMessage("Failed");
				rContentModel.setNpk("");
				rContentModel.setName("");
				rContentModel.setBirthPlace("");
				rContentModel.setBirthDate("");
				rContentModel.setKtpAddress("");
				rContentModel.setDomicileAddress("");
				rContentModel.setReligion("");
				rContentModel.setMaritalStatus("");
				rContentModel.setBloodType("");
				rContentModel.setGender("");
				rContentModel.setPhoneNumber("");
				rContentModel.setEmail("");
				rContentModel.setIdEducation("");
			}
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			rModel.setDate(("" + year + "-" + month + "-" + day).toString());
			rModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			rModel.setContent(rContentModel);
			
			response = mapper.writeValueAsString(rModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}
		return response;
	}
}
