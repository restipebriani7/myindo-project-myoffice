package com.myindo.project.inquirydetaillistubahdatakrywhr.model;

public class ResponseContentModel {
	private String npk;
	private String name;
	private String birthPlace;
	private String birthDate;
	private String ktpAddress;
	private String domicileAddress;
	private String religion;
	private String maritalStatus;
	private String bloodType;
	private String gender;
	private String phoneNumber;
	private String email;
	private String idEducation;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getKtpAddress() {
		return ktpAddress;
	}
	public void setKtpAddress(String ktpAddress) {
		this.ktpAddress = ktpAddress;
	}
	public String getDomicileAddress() {
		return domicileAddress;
	}
	public void setDomicileAddress(String domicileAddress) {
		this.domicileAddress = domicileAddress;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getBloodType() {
		return bloodType;
	}
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdEducation() {
		return idEducation;
	}
	public void setIdEducation(String idEducation) {
		this.idEducation = idEducation;
	}
}
