/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.processor;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquirydetaillistpengajuancuti.dao.GetDetailKaryawan;
import com.myindo.project.inquirydetaillistpengajuancuti.dao.GetDetailListPCuti;
import com.myindo.project.inquirydetaillistpengajuancuti.model.HasilDataKaryawan;
import com.myindo.project.inquirydetaillistpengajuancuti.model.HasilPengajuanCuti;
import com.myindo.project.inquirydetaillistpengajuancuti.model.RequestModel;
import com.myindo.project.inquirydetaillistpengajuancuti.model.ResponseContentModel;
import com.myindo.project.inquirydetaillistpengajuancuti.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProcessInquiryDetailCuti implements Processor {
	Logger log = Logger.getLogger("ProcessInquiryDetailCuti");
	ObjectMapper mapper = new ObjectMapper();
	String codeResponse;
	

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		
		GetDetailListPCuti gCuti = new GetDetailListPCuti();
		GetDetailKaryawan detailKaryawan = new GetDetailKaryawan();
		
		String codeResponseCuti = gCuti.inquiryDetailCuti(rModel);
		String codeResponseKaryawan = detailKaryawan.inquiryKaryawan(rModel);
		
		HasilPengajuanCuti hasilCuti = gson.fromJson(codeResponseCuti, HasilPengajuanCuti.class);
		HasilDataKaryawan hasilKaryawan = gson.fromJson(codeResponseKaryawan, HasilDataKaryawan.class);
				
		String str = response(hasilCuti, hasilKaryawan);
		Response response = Response.status(HttpStatus.OK_200).entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(HasilPengajuanCuti hasilCuti, HasilDataKaryawan hasilKaryawan) throws Exception {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel rModel1 = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();
			if (!hasilCuti.getId_pengajuan().isEmpty() && hasilCuti != null) {
				rModel1.setResponseCode("0000");
				rModel1.setResponseMessage("Success");
				rContentModel.setName(hasilKaryawan.getName());
				rContentModel.setPosition(hasilKaryawan.getPosition());
				rContentModel.setDepartment(hasilKaryawan.getDepartment());
				rContentModel.setSisa_cuti(hasilKaryawan.getSisa_cuti());
				rContentModel.setStatus(hasilCuti.getStatus_pengajuan());
				rContentModel.setDateOfPaidLeave(hasilCuti.getTgl_cuti());
				rContentModel.setTotalDays(hasilCuti.getJml_hari());
				rContentModel.setDateOfEntry(hasilCuti.getTgl_masuk());
				rContentModel.setReason(hasilCuti.getKet_cuti());
				rContentModel.setNpk(hasilCuti.getNpk());
				rContentModel.setCc_atasan(hasilCuti.getCc_atasan());
				rContentModel.setCc_hr(hasilCuti.getCc_hr());
			} else {
				rModel1.setResponseCode("1111");
				rModel1.setResponseMessage("Failed");
				rContentModel.setName("");
				rContentModel.setPosition("");
				rContentModel.setDepartment("");
				rContentModel.setStatus("");
				rContentModel.setDateOfPaidLeave("");
				rContentModel.setTotalDays("");
				rContentModel.setDateOfEntry("");
				rContentModel.setReason("");
				rContentModel.setNpk("");		
				rContentModel.setCc_atasan("");	
				rContentModel.setCc_hr("");
			}
			System.out.println("rContentModel : "+rContentModel.toString());
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			rModel1.setDate(("" + year + "-" + month + "-" + day).toString());
			rModel1.setTime(("" + hour + ":" + minute + ":" + second).toString());
			rModel1.setContent(rContentModel);
			
			response = mapper.writeValueAsString(rModel1);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}
		return response;
	}
}
