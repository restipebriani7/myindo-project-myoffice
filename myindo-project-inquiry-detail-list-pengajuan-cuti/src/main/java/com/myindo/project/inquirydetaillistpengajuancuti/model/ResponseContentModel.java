 /**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String name;
	private String position;
	private String department;
	private String status;
	private String dateOfPaidLeave;
	private String totalDays;
	private String dateOfEntry;
	private String reason;
	private String npk;
	private String cc_atasan;
	private String cc_hr;
	private String sisa_cuti;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateOfPaidLeave() {
		return dateOfPaidLeave;
	}
	public void setDateOfPaidLeave(String dateOfPaidLeave) {
		this.dateOfPaidLeave = dateOfPaidLeave;
	}
	public String getTotalDays() {
		return totalDays;
	}
	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getCc_atasan() {
		return cc_atasan;
	}
	public void setCc_atasan(String cc_atasan) {
		this.cc_atasan = cc_atasan;
	}
	public String getCc_hr() {
		return cc_hr;
	}
	public void setCc_hr(String cc_hr) {
		this.cc_hr = cc_hr;
	}
	public String getSisa_cuti() {
		return sisa_cuti;
	}
	public void setSisa_cuti(String sisa_cuti) {
		this.sisa_cuti = sisa_cuti;
	}
	@Override
	public String toString() {
		return "ResponseContentModel [name=" + name + ", position=" + position + ", department=" + department
				+ ", status=" + status + ", dateOfPaidLeave=" + dateOfPaidLeave + ", totalDays=" + totalDays
				+ ", dateOfEntry=" + dateOfEntry + ", reason=" + reason + ", npk=" + npk + ", cc_atasan=" + cc_atasan
				+ ", cc_hr=" + cc_hr + ", sisa_cuti=" + sisa_cuti + "]";
	}
}
