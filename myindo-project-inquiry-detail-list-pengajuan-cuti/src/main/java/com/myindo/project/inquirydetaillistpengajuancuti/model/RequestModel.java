/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String id_pengajuan;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getId_pengajuan() {
		return id_pengajuan;
	}
	public void setId_pengajuan(String id_pengajuan) {
		this.id_pengajuan = id_pengajuan;
	}
}
