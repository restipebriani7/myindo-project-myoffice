/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirydetaillistpengajuancuti.connection.Connect;
import com.myindo.project.inquirydetaillistpengajuancuti.model.HasilDataKaryawan;
import com.myindo.project.inquirydetaillistpengajuancuti.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetDetailKaryawan {
	Logger log = Logger.getLogger("GetDetailKaryawan");
	String hasilGet;
	HasilDataKaryawan hKaryawan;
	
	public String inquiryKaryawan(RequestModel rModel) throws Exception{
		hKaryawan = new HasilDataKaryawan();
		Connection con = Connect.connection();
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			ps.setString(1, rModel.getNpk());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hKaryawan.setNpk(rs.getString("npk"));
				hKaryawan.setName(rs.getString("nama"));
				hKaryawan.setPosition(rs.getString("id_jabatan"));
				hKaryawan.setDepartment(rs.getString("id_department"));
				hKaryawan.setSisa_cuti(rs.getString("total_cuti"));
			}
			rs.close();
			
			System.out.println("Npk : " +rModel.getNpk());
			
			if (!hKaryawan.getNpk().trim().isEmpty() && hKaryawan.getNpk() != null) {
				ObjectMapper mapper = new ObjectMapper();
				hasilGet = mapper.writeValueAsString(hKaryawan);
				System.out.println("Hasil Get Karyawan : "+hasilGet);
				
				log.info("Select data karyawan berdasarkan npk berhasil");
				con.close();
			} else {
				log.info("Select data gagal else");
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Select data gagal");
			log.info(e.getMessage());
			
			hKaryawan.setNpk("");
			hKaryawan.setName("");
			hKaryawan.setPosition("");
			hKaryawan.setDepartment("");
			hKaryawan.setSisa_cuti("");
			
			ObjectMapper mapper = new ObjectMapper();
			hasilGet = mapper.writeValueAsString(hKaryawan);
			System.out.println("Hasil Get catch karyawan : " + hasilGet);
		}
		return hasilGet;
	}
}
