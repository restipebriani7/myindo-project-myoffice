/**
 * 
 */
package com.myindo.project.transferantarkaryawan.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.transferantarkaryawan.dao.TransferKaryawan;
import com.myindo.project.transferantarkaryawan.model.RequestModel;
import com.myindo.project.transferantarkaryawan.model.ResponseContentModel;
import com.myindo.project.transferantarkaryawan.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesTransfer implements Processor {
	Logger log = Logger.getLogger("ProsesTransfer");
	ObjectMapper mapper =new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel reModel = gson.fromJson(body, RequestModel.class);

		TransferKaryawan tfKary = new TransferKaryawan();
		String hasil = tfKary.tfKaryawan(reModel);

		String res = response(hasil);
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String hasil) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel = new ResponseContentModel();
			
			if (hasil.equals("0000")) {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				contentModel.setMessage("Poin berhasil di transfer");
			} else {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setMessage("Poin gagal di transfer");
			}
			
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			contentModel.setCode(hasil);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
