/**
 * 
 */
package com.myindo.project.transferantarkaryawan.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGetTerima {
	private String nama;
	private String npk;
	private String totalPoin;
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getTotalPoin() {
		return totalPoin;
	}
	public void setTotalPoin(String totalPoin) {
		this.totalPoin = totalPoin;
	}
	@Override
	public String toString() {
		return "HasilGetTerima [nama=" + nama + ", npk=" + npk + ", totalPoin=" + totalPoin + "]";
	}
}
