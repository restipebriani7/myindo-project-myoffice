/**
 * 
 */
package com.myindo.project.transferantarkaryawan.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String amount;
	private String to;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
}
