/**
 * 
 */
package com.myindo.project.transferantarkaryawan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.transferantarkaryawan.connection.Connect;
import com.myindo.project.transferantarkaryawan.model.HasilGet;
import com.myindo.project.transferantarkaryawan.model.HasilGetTerima;
import com.myindo.project.transferantarkaryawan.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class TransferKaryawan {
	Logger log = Logger.getLogger("TransferKaryawan");
	ObjectMapper mapper = new ObjectMapper();
	HasilGet listGet ;
	HasilGetTerima listPenerima ;
	String hasilGet;
	String codeResponse;
	
	public String tfKaryawan (RequestModel rModel) {
		Connection con = Connect.connection();
		listGet = new HasilGet();
		listPenerima = new HasilGetTerima();
		
		try {
			PreparedStatement pp = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			pp.setString(1, rModel.getNpk());
			ResultSet rs = pp.executeQuery();
			while (rs.next()) {
				listGet.setNpk(rs.getString("npk"));
				listGet.setTotalPoin(rs.getString("total_poin"));
			}
			rs.close();
			System.out.println("Npk : " +rModel.getNpk());
			
			PreparedStatement pStat = con.prepareStatement("SELECT * FROM karyawan WHERE nama=?");
			pStat.setString(1, rModel.getTo());
			ResultSet rSult = pStat.executeQuery();
			while (rSult.next()) {
				listPenerima.setNpk(rSult.getString("npk"));
				listPenerima.setNama(rSult.getString("nama"));
				listPenerima.setTotalPoin(rSult.getString("total_poin"));
			}
			rs.close();
			
			codeResponse = "0000";
			log.info("Select data berhasil");
			hasilGet = mapper.writeValueAsString(listGet);
			System.out.println("Hasil Get tb karyawan : "+hasilGet);

			int poinPenerima = Integer.parseInt(listPenerima.getTotalPoin());
			int totPoin = Integer.parseInt(listGet.getTotalPoin());
			int rePoin = Integer.parseInt(rModel.getAmount());
			
			int jmlPenerima = poinPenerima + rePoin;
			int jmlPoin = totPoin - rePoin;
			
			String jmlPenerimaString = Integer.toString(jmlPenerima);
			String jmlPoinString = Integer.toString(jmlPoin);
			
			System.out.println("Sisa Poin : "+jmlPoin);
			
			if (jmlPoin >=  0 && !listPenerima.getNama().trim().isEmpty()) {
				PreparedStatement pq = con.prepareStatement("UPDATE karyawan SET total_poin=? WHERE npk=?");
				pq.setString(1, jmlPoinString);
				pq.setString(2, rModel.getNpk());
				pq.executeUpdate();
				
				PreparedStatement ps = con.prepareStatement("UPDATE karyawan SET total_poin=? WHERE nama=?");
				ps.setString(1, jmlPenerimaString);
				ps.setString(2, rModel.getTo());
				ps.executeUpdate();
				
				codeResponse = "0000";
				log.info("Poin berhasil di transfer ke penerima");
			} else {
				codeResponse = "1111";
				log.info("Poin yang di transfer melebihi total poin");	
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Transfer poin gagal");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
