/**
 * 
 */
package com.myindo.project.transferantarkaryawan.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String npk;
	private String totalPoin;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getTotalPoin() {
		return totalPoin;
	}
	public void setTotalPoin(String totalPoin) {
		this.totalPoin = totalPoin;
	}
	@Override
	public String toString() {
		return "HasilGet [npk=" + npk + ", totalPoin=" + totalPoin + "]";
	}
}
