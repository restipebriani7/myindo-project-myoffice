/**
 * 
 */
package com.myindo.project.inquirydetailproduct.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String idProduct;
	private String status;

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
