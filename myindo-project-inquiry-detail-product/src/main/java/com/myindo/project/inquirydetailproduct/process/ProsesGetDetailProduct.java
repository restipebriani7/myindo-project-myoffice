/**
 * 
 */
package com.myindo.project.inquirydetailproduct.process;


import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.inquirydetailproduct.dao.GetDetailProduct;
import com.myindo.project.inquirydetailproduct.model.RequestModel;
import com.myindo.project.inquirydetailproduct.model.ResponseContentModel;
import com.myindo.project.inquirydetailproduct.model.ResponseModel;
import com.myindo.project.inquirydetailproduct.model.ResultGetProduct;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesGetDetailProduct implements Processor{
	Logger log = Logger.getLogger("ProsesGetDetailProduct");
	ObjectMapper mapper = new ObjectMapper();
	String encodstring;

	private FileInputStream fileInputStreamReader;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		GetDetailProduct gProduct = new GetDetailProduct();

		String codeResponse = gProduct.getDetailProduct(requestModel.getIdProduct());
		
		ResultGetProduct rProduct = gson.fromJson(codeResponse, ResultGetProduct.class);

		String foto = rProduct.getPicture();
		File f = new File(foto);
		encodstring = encodeFileToBase64Binary(f);
//		System.out.println("Encodstring : "+encodstring);
		
		String ser = response(rProduct);
		Response response = Response.status(HttpStatus.OK_200).entity(ser)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String encodeFileToBase64Binary(File file) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Encode File To Base64 Binary");
        fileInputStreamReader = new FileInputStream(file);
        byte[] bytes = new byte[(int)file.length()];
        fileInputStreamReader.read(bytes);
        System.out.println("Sukses encode");
        return new String(Base64.encodeBase64(bytes), "UTF-8");
	}

	private String response(ResultGetProduct rProduct) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();

			if (rProduct.getProductName().trim().isEmpty()) {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				rContentModel.setProductName("");
				rContentModel.setAmount("");
				rContentModel.setStart_date("");
				rContentModel.setEnd_date("");
				rContentModel.setPicture("");
				rContentModel.setStatus("");
				rContentModel.setDescription("");
			} else {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				rContentModel.setProductName(rProduct.getProductName());
				rContentModel.setAmount(rProduct.getAmount());
				rContentModel.setStart_date(rProduct.getStart_date());
				rContentModel.setEnd_date(rProduct.getEnd_date());
				rContentModel.setPicture(encodstring);
				rContentModel.setStatus(rProduct.getStatus());
				rContentModel.setDescription(rProduct.getDescription());
			}
			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int mount = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);

			responseModel.setDate(("" + year + "-" + mount + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(rContentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
