/**
 * 
 */
package com.myindo.project.inquirydetailproduct.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirydetailproduct.connection.Connect;
import com.myindo.project.inquirydetailproduct.model.ResultGetProduct;

/**
 * @author Resti Pebriani
 *
 */
public class GetDetailProduct {
	Logger log = Logger.getLogger("GetDetailProduct");
	ResultGetProduct product;
	String hasilProduct;

	public String getDetailProduct(String idProduct) throws Exception {
		Connection con = Connect.connection();
		product = new ResultGetProduct();

		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM product WHERE id_product=?");
			ps.setString(1, idProduct);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				product.setIdProduct(rs.getString("id_product"));
				product.setProductName(rs.getString("product_name"));
				product.setAmount(rs.getString("amount"));
				product.setStart_date(rs.getString("start_date"));
				product.setEnd_date(rs.getString("end_date"));
				product.setPicture(rs.getString("picture"));
				product.setStatus(rs.getString("status"));
				product.setDescription(rs.getString("description"));
			}
			rs.close();
			System.out.println("ID Product : " + idProduct);

			if (!product.getIdProduct().trim().isEmpty() && product.getIdProduct() != null) {
				ObjectMapper mapper = new ObjectMapper();
				hasilProduct = mapper.writeValueAsString(product);
				System.out.println("Hasil get product : " + hasilProduct);
				log.info("Inquiry detail product sukses");
				con.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Inquiry detail product Failed");
			log.info(e.getMessage());
			product = new ResultGetProduct();

			product.setProductName("");
			product.setAmount("");
			product.setStart_date("");
			product.setEnd_date("");
			product.setPicture("");
			product.setStatus("");

			ObjectMapper mapper = new ObjectMapper();
			hasilProduct = mapper.writeValueAsString(product);
			System.out.println("Hasil catch product : " + hasilProduct);
		}
		return hasilProduct;
	}
}
