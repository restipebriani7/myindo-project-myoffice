/**
 * 
 */
package com.myindo.project.approvedatakaryawanhr.processor;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.approvedatakaryawanhr.dao.GetDataKaryawanHr;
import com.myindo.project.approvedatakaryawanhr.model.RequestModel;
import com.myindo.project.approvedatakaryawanhr.model.ResponseContentModel;
import com.myindo.project.approvedatakaryawanhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProccesApproveDataKaryawanHr implements Processor{

	ObjectMapper mapper = new ObjectMapper();
	Logger log = Logger.getLogger("ProccesApproveDataKaryawanHr");
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		
		GetDataKaryawanHr geHr = new GetDataKaryawanHr();
		
		String codeResponse = geHr.approveDb(requestModel.getNpk());
		System.out.println("CodeResponse : " +codeResponse);
		
		String strReq = response(codeResponse);
		
		Response response = Response.status(HttpStatus.OK_200)
				.entity(strReq)
				.header(Exchange.CONTENT_TYPE, "application/json")
				.build();
		exchange.getOut().setBody(response);	
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel =new ResponseContentModel();
			
			if(codeResponse.equals("0000")) {
				responseModel.setResponseMessage("Success");
				contentModel.setCode("0000");
				contentModel.setMessage("Data Berhasil di Approve");				
			}else if (codeResponse.equals("1111")) {
				responseModel.setResponseMessage("Failed");
				contentModel.setCode("1111");
				contentModel.setMessage("Data Gagal di Approve");
			}
			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int mount = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);
			
			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + mount + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(contentModel);
			
			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}
		return response;
	}

}
