/**
 * 
 */
package com.myindo.project.approvedatakaryawanhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.myindo.project.approvedatakaryawanhr.connection.Connect;
import com.myindo.project.approvedatakaryawanhr.model.GetModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetDataKaryawanHr {

	Logger log = Logger.getLogger("GetDataKaryawanHr");

	public String approveDb(String npk) throws Exception {
		// TODO Auto-generated method stub
		String codeResponse = "";
		
		GetModel resultDb = new GetModel();

		try {
			Connection con = Connect.connection();
			PreparedStatement ps = con.prepareStatement("SELECT * from karyawan_temp WHERE npk = ?");
			ps.setString(1, npk);
			ResultSet st = ps.executeQuery();

			while (st.next()) {
				resultDb.setNpk(st.getString("npk"));
				resultDb.setName(st.getString("nama"));
				resultDb.setBirthPlace(st.getString("tmpt_lahir"));
				resultDb.setBirthDate(st.getString("tgl_lahir"));
				resultDb.setKtpAddress(st.getString("alamat_ktp"));
				resultDb.setDomicileAddress(st.getString("alamat_skrg"));
				resultDb.setReligion(st.getString("agama"));
				resultDb.setMaritalStatus(st.getString("status_pernikahan"));
				resultDb.setGender(st.getString("jns_kelamin"));
				resultDb.setBloodType(st.getString("gol_darah"));
				resultDb.setPhoneNumber(st.getString("no_telp"));
				resultDb.setEmail(st.getString("email"));
				resultDb.setIdEducation(st.getString("id_pendidikan"));
			}	
			st.close();
			log.info("Select data is success");
			
			if (!resultDb.getNpk().trim().isEmpty() && resultDb.getNpk() != null) {
				PreparedStatement pp = con.prepareStatement("UPDATE karyawan SET nama=?, tmpt_lahir=?, tgl_lahir=?, alamat_ktp=?, alamat_skrg=?, agama=?, status_pernikahan=?, "
						+ "jns_kelamin=?, gol_darah=?, no_telp=?, email=?, id_pendidikan=? WHERE npk=?");

				pp.setString(1, resultDb.getName());
				pp.setString(2, resultDb.getBirthPlace());
				pp.setString(3, resultDb.getBirthDate());
				pp.setString(4, resultDb.getKtpAddress());
				pp.setString(5, resultDb.getDomicileAddress());
				pp.setString(6, resultDb.getReligion());
				pp.setString(7, resultDb.getMaritalStatus());
				pp.setString(8, resultDb.getGender());
				pp.setString(9, resultDb.getBloodType());
				pp.setString(10, resultDb.getPhoneNumber());
				pp.setString(11, resultDb.getEmail());
				pp.setString(12, resultDb.getIdEducation());
				pp.setString(13, resultDb.getNpk());

				pp.executeUpdate();
				codeResponse = "0000";
				log.info("Update Data is Success");
				
				if (codeResponse.equals("0000")) {
					PreparedStatement rr = con.prepareStatement("DELETE FROM karyawan_temp WHERE npk=?");
					rr.setString(1, npk);
					rr.execute();
					log.info("Delete Data is Success");
				}
				log.info("Approve Data is Success");
			} else {
				codeResponse = "1111";
				log.info("Data is Null");
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Approve Data is Failed");
			log.info(e.getMessage());
			codeResponse = "1111";
		}
		return codeResponse;
	}
}
