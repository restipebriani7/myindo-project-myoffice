/**
 * 
 */
package com.myindo.project.approvedatakaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {

	private String npk;
	
	public String getNpk() {
		return npk;
	}

	public void setNpk(String npk) {
		this.npk = npk;
	}

}
