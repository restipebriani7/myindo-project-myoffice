/**
 * 
 */
package com.myindo.project.approvedatakaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String code;
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
