/**
 * 
 */
package com.myindo.project.approvedatakaryawanhr.model;


/**
 * @author Resti Pebriani
 *
 */
public class GetModel {
	private String npk;
	private String photo;
	private String name;
	private String employeeStatus;
	private String joinDate;
	private String resignDate;
	private String position;
	private String department;
	private String birthPlace;
	private String birthDate;
	private String ktpAddress;
	private String domicileAddress;
	private String religion;
	private String maritalStatus;
	private String bloodType;
	private String gender;
	private String phoneNumber;
	private String email;
	private String npwp;
	private String point;
	private String paidLeaveRemain;
	private String idInsurance;
	private String idSavings;
	private String idEducation;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployeeStatus() {
		return employeeStatus;
	}
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}
	public String getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}
	public String getResignDate() {
		return resignDate;
	}
	public void setResignDate(String resignDate) {
		this.resignDate = resignDate;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getKtpAddress() {
		return ktpAddress;
	}
	public void setKtpAddress(String ktpAddress) {
		this.ktpAddress = ktpAddress;
	}
	public String getDomicileAddress() {
		return domicileAddress;
	}
	public void setDomicileAddress(String domicileAddress) {
		this.domicileAddress = domicileAddress;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getBloodType() {
		return bloodType;
	}
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getPaidLeaveRemain() {
		return paidLeaveRemain;
	}
	public void setPaidLeaveRemain(String paidLeaveRemain) {
		this.paidLeaveRemain = paidLeaveRemain;
	}
	public String getIdInsurance() {
		return idInsurance;
	}
	public void setIdInsurance(String idInsurance) {
		this.idInsurance = idInsurance;
	}
	public String getIdSavings() {
		return idSavings;
	}
	public void setIdSavings(String idSavings) {
		this.idSavings = idSavings;
	}
	public String getIdEducation() {
		return idEducation;
	}
	public void setIdEducation(String idEducation) {
		this.idEducation = idEducation;
	}
	
}
