/**
 * 
 */
package com.myindo.project.inquirylistredeem.model;

import java.util.List;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseModel {
	private String responseCode;
	private String responseMessage;
	private String date;
	private String time;
	private List<ResponseContentModel> content;
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public List<ResponseContentModel> getContent() {
		return content;
	}
	public void setContent(List<ResponseContentModel> content) {
		this.content = content;
	}
}
