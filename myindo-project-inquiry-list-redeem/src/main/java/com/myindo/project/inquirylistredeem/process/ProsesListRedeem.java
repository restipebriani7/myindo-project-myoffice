/**
 * 
 */
package com.myindo.project.inquirylistredeem.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirylistredeem.dao.GetListRedeem;
import com.myindo.project.inquirylistredeem.model.HasilListRedeem;
import com.myindo.project.inquirylistredeem.model.ResponseContentModel;
import com.myindo.project.inquirylistredeem.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesListRedeem implements Processor {
	Logger log = Logger.getLogger("ProsesListRedeem");
	ObjectMapper mapper = new ObjectMapper();
	String codeResponse;

	GetListRedeem list = new GetListRedeem();
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<HasilListRedeem> hasilGets = list.inquiryListRedeem();
		log.info("Array List Redeem : " +hasilGets);

		if (hasilGets.isEmpty()) {
			codeResponse ="1111";
			log.info("Get data gagal");
		} else{
			codeResponse ="0000";
			log.info("Get data berhasil");
		}
		
		String str = response(codeResponse, hasilGets);
		Response response = Response.status(HttpStatus.OK_200).entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse, ArrayList<HasilListRedeem> hasilGets) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel rModel = new ResponseModel();
			ArrayList<ResponseContentModel> contentModel = new ArrayList<>();

			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilGets.size(); i++) {
					ResponseContentModel model = new ResponseContentModel();
					rModel.setResponseMessage("Success");
					model.setNpk(hasilGets.get(i).getNpk());
					model.setIdProduct(hasilGets.get(i).getIdProduct());
					model.setDateTransaction(hasilGets.get(i).getDateTransaction());
					model.setStatus(hasilGets.get(i).getStatus());
					contentModel.add(model);
				}
				
			} else {
				ResponseContentModel model2 = new ResponseContentModel();
				rModel.setResponseMessage("Failed");
				model2.setNpk("");
				model2.setIdProduct("");
				model2.setDateTransaction("");
				model2.setStatus("");
				
				contentModel.add(model2);
			}
			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int month = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);
			
			rModel.setResponseCode(codeResponse);
			rModel.setDate(("" + year + "-" + month + "-" + day).toString());
			rModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			rModel.setContent(contentModel);

			response = mapper.writeValueAsString(rModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
