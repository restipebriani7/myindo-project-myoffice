/**
 * 
 */
package com.myindo.project.inquirylistredeem.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilListRedeem {
	private String npk;
	private String idProduct;
	private String dateTransaction;
	private String status;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public String getDateTransaction() {
		return dateTransaction;
	}
	public void setDateTransaction(String dateTransaction) {
		this.dateTransaction = dateTransaction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "HasilListRedeem [npk=" + npk + ", idProduct=" + idProduct + ", dateTransaction=" + dateTransaction
				+ ", status=" + status + "]";
	}
	
}
