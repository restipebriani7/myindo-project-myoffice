/**
 * 
 */
package com.myindo.project.inquirylistredeem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.inquirylistredeem.connection.Connect;
import com.myindo.project.inquirylistredeem.model.HasilListRedeem;

/**
 * @author Resti Pebriani
 *
 */
public class GetListRedeem {
	Logger log = Logger.getLogger("GetListRedeem");
	HasilListRedeem listRedeem;

	public ArrayList<HasilListRedeem> inquiryListRedeem() throws Exception {
		ArrayList<HasilListRedeem> hasilArray = new ArrayList<>();
		Connection con = Connect.connection();
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM list_redeem");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listRedeem = new HasilListRedeem();
				listRedeem.setNpk(rs.getString("npk"));
				listRedeem.setIdProduct(rs.getString("id_product"));
				listRedeem.setDateTransaction(rs.getString("date_trans"));
				listRedeem.setStatus(rs.getString(5));
				
				hasilArray.add(listRedeem);
			}
			rs.close();
			System.out.println("Hasil list redeem : "+hasilArray.toString());
			log.info("Berhasil");
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Gagal");
			log.info(e.getMessage());
			
			listRedeem.setNpk("");
			listRedeem.setIdProduct("");
			listRedeem.setDateTransaction("");	
			listRedeem.setStatus("");
	}
		return hasilArray;
	}
}
