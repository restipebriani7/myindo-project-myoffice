/**
 * 
 */
package com.myindo.project.addproduct.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.addproduct.dao.AddProduct2;
import com.myindo.project.addproduct.model.RequestModel;
import com.myindo.project.addproduct.model.ResponseContentModel;
import com.myindo.project.addproduct.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesAddProduct implements Processor{
	Logger log = Logger.getLogger("ProsesAddProduct");
	ObjectMapper mapper = new ObjectMapper();
	String uploadFile = "";

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		AddProduct2 add = new AddProduct2();

		String codeResponse = add.addProduct(rModel);

		System.out.println("CodeResponse : " + codeResponse);

		String str = response(codeResponse);
		Response response = Response.status(HttpStatus.OK_200).entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel = new ResponseContentModel();

			if (codeResponse.equals("0000")) {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Succes");
				contentModel.setMessage("Product berhasil ditambahkan");
			} else if (codeResponse.equals("1111")) {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setMessage("Product gagal ditambahkan");
			}

			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			contentModel.setCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}

}
