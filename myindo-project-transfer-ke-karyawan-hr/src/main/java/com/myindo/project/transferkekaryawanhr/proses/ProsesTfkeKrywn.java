/**
 * 
 */
package com.myindo.project.transferkekaryawanhr.proses;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.transferkekaryawanhr.dao.TransferKeKaryawan;
import com.myindo.project.transferkekaryawanhr.model.RequestModel;
import com.myindo.project.transferkekaryawanhr.model.ResponseContentModel;
import com.myindo.project.transferkekaryawanhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesTfkeKrywn implements Processor{
	Logger log = Logger.getLogger("ProsesTfkeKrywn");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel reModel = gson.fromJson(body, RequestModel.class);
		TransferKeKaryawan tfKar = new TransferKeKaryawan();
		
		String codeResponse = tfKar.tfkeKaryawan(reModel);
		
		String res = response(codeResponse);
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "" ;
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel = new ResponseContentModel();
			
			if (codeResponse.equals("0000")) {
				responseModel.setResponseMessage("Success");
				contentModel.setMessage("Poin berhasil di transfer ke karyawan oleh HR");
			} else {
				responseModel.setResponseMessage("Failed");
				contentModel.setMessage("Poin gagal di transfer ke karyawan oleh HR");
			}
			
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
