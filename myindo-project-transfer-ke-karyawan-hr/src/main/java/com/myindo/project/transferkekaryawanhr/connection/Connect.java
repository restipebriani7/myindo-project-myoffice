/**
 * 
 */
package com.myindo.project.transferkekaryawanhr.connection;

import java.sql.Connection;

import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myindo.project.transferkekaryawanhr.model.ParameterDB;
import com.myindo.project.transferkekaryawanhr.util.ParameterizedDB;


/**
 * @author Resti Pebriani
 *
 */
public class Connect {
	static Logger log = LoggerFactory.getLogger(Connect.class);
	public static Connection conn;
	public static Connection connection() {
		try {
			ParameterDB parameterDB = ParameterizedDB.getPropeties();
			PGSimpleDataSource source = new PGSimpleDataSource();
			
			source.setDatabaseName(parameterDB.getDb());
			source.setServerName(parameterDB.getEndPoint());
			source.setPortNumber(Integer.parseInt(parameterDB.getPort()));
			source.setUser(parameterDB.getUsername());
			source.setPassword(parameterDB.getPassword());
			
			conn = source.getConnection();
			
			log.info("Connect is Success");
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Connect is Failed");
			log.info("EXCEPTION : " +e.getMessage());
		}		
		return conn;
	}
}
