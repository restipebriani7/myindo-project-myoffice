/**
 * 
 */
package com.myindo.project.transferkekaryawanhr.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilPointHr {
	private String idPoinHr;
	private String npk;
	private String totalPoin;
	public String getIdPoinHr() {
		return idPoinHr;
	}
	public void setIdPoinHr(String idPoinHr) {
		this.idPoinHr = idPoinHr;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getTotalPoin() {
		return totalPoin;
	}
	public void setTotalPoin(String totalPoin) {
		this.totalPoin = totalPoin;
	}
	@Override
	public String toString() {
		return "HasilPointHr [idPoinHr=" + idPoinHr + ", npk=" + npk + ", totalPoin=" + totalPoin + "]";
	}
}
