/**
 * 
 */
package com.myindo.project.approvecutihr.process;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.approvecutihr.dao.GetCutiHr;
import com.myindo.project.approvecutihr.model.RequestModel;
import com.myindo.project.approvecutihr.model.ResponseContentModel;
import com.myindo.project.approvecutihr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesApproveCutiHr implements Processor {
	Logger log = Logger.getLogger("ProsesApproveCutiHr");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		GetCutiHr gHr = new GetCutiHr();

		String codeResponse = gHr.approveCuti(rModel);

		System.out.println("CodeResponse : " + codeResponse);

		String str = response(codeResponse);
		Response response = Response.status(HttpStatus.OK_200).entity(str)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel rModel = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();
			if (codeResponse.equals("0000")) {
				rModel.setResponseCode("0000");
				rModel.setResponseMessage("Success");
				rContentModel.setMessage("Pengajuan cuti di terima");
			} else if (codeResponse.equals("0011")) {
				rModel.setResponseCode("0011");
				rModel.setResponseMessage("Success");
				rContentModel.setMessage("Pengajuan cuti di tolak");
			} else {
				rModel.setResponseCode("1111");
				rModel.setResponseMessage("Failed");
				rContentModel.setMessage("Proses Aproval gagal");
			}
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);
			
			rContentModel.setCode(codeResponse);
			rModel.setDate(("" + year + "-" + month + "-" + day).toString());
			rModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			rModel.setContent(rContentModel);

			response = mapper.writeValueAsString(rModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace() + "");
		}
		return response;
	}
}
