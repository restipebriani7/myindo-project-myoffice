/**
 * 
 */
package com.myindo.project.approvecutihr.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String id_pengajuan;
	private String status;
	private String total_cuti;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getId_pengajuan() {
		return id_pengajuan;
	}
	public void setId_pengajuan(String id_pengajuan) {
		this.id_pengajuan = id_pengajuan;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTotal_cuti() {
		return total_cuti;
	}
	public void setTotal_cuti(String total_cuti) {
		this.total_cuti = total_cuti;
	}
}
