/**
 * 
 */
package com.myindo.project.approvecutihr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.myindo.project.approvecutihr.connection.Connect;
import com.myindo.project.approvecutihr.model.HasilGet;
import com.myindo.project.approvecutihr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetCutiHr {
	Logger log = Logger.getLogger("GetCutiHr");
	String codeResponse = "";
	String status = "";

	public String approveCuti(RequestModel rModel) throws Exception {
		Connection con = Connect.connection();
		HasilGet hasilGet = new HasilGet();

		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM pengajuan_cuti WHERE id_pengajuan=?");
			ps.setString(1, rModel.getId_pengajuan());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hasilGet.setIdPengajuan(rs.getString(1));
				hasilGet.setNpk(rs.getString("npk"));
				hasilGet.setStatus(rs.getString(2));
				hasilGet.setJml_hari(rs.getString(4));
			}
			System.out.println("ID pengajuan : "+hasilGet.getIdPengajuan());
			System.out.println("Npk : " +hasilGet.getNpk());
			System.out.println("Status di DB: "+hasilGet.getStatus());
			rs.close();
			
			PreparedStatement pStment = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			pStment.setString(1, rModel.getNpk());
			ResultSet rSet = pStment.executeQuery();

			while (rSet.next()) {
				hasilGet.setTotal_cuti(rSet.getString("total_cuti"));
			}
			System.out.println("Total cuti : "+hasilGet.getTotal_cuti());
			rSet.close();
			
			int total_cuti = Integer.parseInt(hasilGet.getTotal_cuti());
			int req_cuti = Integer.parseInt(hasilGet.getJml_hari());
			int sisa_cuti = total_cuti - req_cuti;
			String sisa_cuti_string = Integer.toString(sisa_cuti);

			if (!hasilGet.getNpk().equals("") && !hasilGet.getIdPengajuan().equals("") && hasilGet.getStatus().equals("3") && rModel.getStatus().equals("1")) {
				status = "1";
				PreparedStatement pStm = con
						.prepareStatement("UPDATE pengajuan_cuti SET status_pengajuan=? WHERE id_pengajuan=?");
				pStm.setString(1, status);
				pStm.setString(2, rModel.getId_pengajuan());

				pStm.executeUpdate();

				codeResponse = "0000";
				log.info("Proses di approve");
			}
			else if (!hasilGet.getNpk().equals("") && !hasilGet.getIdPengajuan().equals("") && hasilGet.getStatus().equals("3") && rModel.getStatus().equals("4")) {
				status = "4";
				PreparedStatement pp = con.prepareStatement("UPDATE pengajuan_cuti SET status_pengajuan=? WHERE id_pengajuan=?");
				pp.setString(1, status);
				pp.setString(2, rModel.getId_pengajuan());

				pp.executeUpdate();

				codeResponse = "0011";
				log.info("Proses di rejek");
			}
			else if (!hasilGet.getNpk().equals("") && !hasilGet.getIdPengajuan().equals("") && hasilGet.getStatus().equals("1")) {
				System.out.println("Pengajuan cuti telah di approve");
			}
			else if (!hasilGet.getNpk().equals("") && !hasilGet.getIdPengajuan().equals("") && hasilGet.getStatus().equals("4")) {
				System.out.println("Pengajuan cuti telah di rejek");
			}
			
			if (codeResponse.equals("0000")) {
				PreparedStatement po = con.prepareStatement("UPDATE karyawan SET total_cuti=? WHERE npk=?");
				po.setString(1, sisa_cuti_string);
				po.setString(2, rModel.getNpk());
				po.executeUpdate();
				
				log.info("Update total cuti ke tabel karyawan berhasil");
			}

			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Proses Failed");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
