/**
 * 
 */
package com.myindo.project.approvecutihr.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilGet {
	private String npk;
	private String status;
	private String idPengajuan;
	private String total_cuti;
	private String jml_hari;

	public String getNpk() {
		return npk;
	}

	public void setNpk(String npk) {
		this.npk = npk;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdPengajuan() {
		return idPengajuan;
	}

	public void setIdPengajuan(String idPengajuan) {
		this.idPengajuan = idPengajuan;
	}

	public String getTotal_cuti() {
		return total_cuti;
	}

	public void setTotal_cuti(String total_cuti) {
		this.total_cuti = total_cuti;
	}

	public String getJml_hari() {
		return jml_hari;
	}

	public void setJml_hari(String jml_hari) {
		this.jml_hari = jml_hari;
	}

	@Override
	public String toString() {
		return "HasilGet [npk=" + npk + ", status=" + status + ", idPengajuan=" + idPengajuan + ", total_cuti="
				+ total_cuti + ", jml_hari=" + jml_hari + "]";
	}
}
